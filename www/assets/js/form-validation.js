Foundation.Abide.defaults.validators['email'] =
    function ($el, required, parent) {
        var regexeml = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)+$/;
        var val = document.getElementById('email').value;
        var length = document.getElementById('email').value.length;
        if (length === 0) {
            $('#error2').html('E-mail is verplicht.');
            return false;
        } else if (!val.match(regexeml)) {
            $('#error2').html('Ongeldig e-mail adres');
            return false;
        }
        return true;
    };

    $(document).foundation()
    .on("formvalid.zf.abide", function(ev,frm) {
        $.ajax({
            url: "save.php",
            method:"POST", //First change type to method here
            data: $('#subscribeForm').serialize(),
            success:function(response) {
             $('#submitbtn').prop('disabled', true);
             if(response.status == "OK" ){
                window.location = response.url;
             }
           },
           error:function(){
            $('#loading').hide();
            $('#submitbtn').prop('disabled', true); 
           }
          });
        
      });