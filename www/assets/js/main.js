'use strict';

$('.main-carousel').flickity({
    // options
    cellAlign: 'left',
    contain: true,
    cellSelector: '.carousel-cell',
    autoPlay: 5000,
    prevNextButtons: false,
    draggable: false
});

$('.flickity-page-dots').wrap("<div class='container'></div>");
