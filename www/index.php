<?php
require_once '../vendor/autoload.php';

$loader = new Twig_Loader_Filesystem('../src/views/');
$twig = new Twig_Environment($loader, array());
$utm = ['utm_medium','utm_campaign','utm_term','utm_source','utm_content'];
echo $twig->render('index.html.twig',array(
    'utm' => $utm,
    'GET' => $_GET
));
