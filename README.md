![alt text](https://www.google.com/a/cpanel/growingminds.nl/images/logo.gif?alpha=1&service=google_default "Growing Minds Logo")


## Commands to get it working.
1. `npm install` (Will install every NPM package)
2. `composer install` (For Twig Files)

## Basic Commands
* `grunt production` (Cleans necessary folders and runs predefined tasks + Minifies files)
* `grunt development` (Cleans necessary folders and runs predefined tasks)
* `grunt watch` (Run Sass Compiler, ES6 Compiler and watches for changes (For Development))

## Dependencies
* PHP
* Composer
* Grunt



Made with Bootstrap 4, jQuery, Sass and ES6.

<strong>

> Made By Peter Boersma <br>
Email: p.boersma@lycan-media.nl <br>
Phone: +31 6 800677 06
</strong>