module.exports = function (grunt) {
    require('load-grunt-tasks')(grunt);
    let fileList = ["src/scss/*","src/js/*"];

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        sass: {
            prod: {
                options: {
                    sourcemap: 'auto',
                    style: 'compressed',
                    update: true
                },
                files: {
                    'www/assets/css/style.min.css': 'src/scss/main.scss',
                }
            },
            dev: {
                options: {
                    sourcemap: 'none',
                    style: 'nested',
                    update: true
                },
                files: {
                    'www/assets/css/style.css': 'src/scss/main.scss',
                }
            }
        },
        babel: {
            prod: {
                options: {
                    sourceMap: true,
                    minified: true,
                    presets: ['env']
                },
                files: {
                    'www/assets/js/main.min.js': 'src/js/main.js'
                }
            },

            dev: {
                options: {
                    presets: ['env']
                },
                files: {
                    'www/assets/js/main.js': 'src/js/main.js'
                }
            }
        },
        clean: ['www/assets/js/main.*','www/assets/css/style.*',],
        watch: {
            scripts: {
                files: fileList,
                tasks: ['sass:dev','babel:dev'],
                options: {
                    spawn: false,
                },
            },
        },

    });


    grunt.registerTask('development', ['clean','sass:dev', 'babel:dev']);
    grunt.registerTask('production', ['clean','sass:prod', 'babel:prod']);



};